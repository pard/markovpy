from PIL import Image
import sys
import random
from functools import reduce
from operator import iconcat


def load_(file):
    return Image.open(file)


def make_rule(data):
    rule = {}
    width, height = data.size
    pixels = data.load()
    for y in range(height):
        for x in range(width):
            key = pixels[x, y]
            colors = []
            if x+1 != width:
                left = pixels[x+1, y]
                colors.append(left)
            if x != 0:
                right = pixels[x-1, y]
                colors.append(right)
            if y+1 != height:
                below = pixels[x, y+1]
                colors.append(below)
            if y != 0:
                above = pixels[x, y-1]
                colors.append(above)
            if key in rule:
                rule[key] += colors
            else:
                rule[key] = colors
    return rule


def neighbors(x, y, canvas):
    width, height = canvas.size
    pixels = canvas.load()
    colors = []
    if x+1 != width:
        left = pixels[x+1, y]
    if x != 0:
        right = pixels[x-1, y]
        colors.append(right)
    if y+1 != height:
        below = pixels[x, y+1]
        colors.append(below)
    if y != 0:
        above = pixels[x, y-1]
    return colors


def make_image(canvas, rule):
    next_pixel = random.choice(list(rule.keys()))
    key = [next_pixel]
    width, height = canvas.size
    pixels = canvas.load()
    for y in range(height):
        for x in range(width):
            if x == 0 and y != 0:
                key = pixels[x, y-1]
                next_pixel = random.choice(rule[key])
                # key = neighbors(x, y-1, canvas)
                # next_pixel = random.choice(reduce(iconcat, key, []))
            pixels[x, y] = next_pixel
            key = next_pixel
            next_pixel = random.choice(rule[key])
            # key = neighbors(x, y-1, canvas)
            # next_pixel = random.choice(reduce(iconcat, key, []))
    return canvas


def main():
    file = sys.argv[1]
    data = load_(file)
    canvas = Image.new(data.mode, data.size)
    rule = make_rule(data)
    new_image = make_image(canvas, rule)
    new_image.save("new.png")


if __name__ == "__main__":
    main()

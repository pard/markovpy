import sys
import random


class Markov:
    def __init__(self, data=""):
        self.words = [x.strip() for x in data.replace('\n', ' ').split(' ')]
        self.context = 3
        self.length = 10
        self.suffixes = ['.', '!', '?']

    def with_context(self, context):
        self.context = context

    def with_length(self, length):
        self.length = length

    def with_suffixes(self, suffixes):
        self.suffixes = suffixes

    def make_rule(self):
        rule = {'__endings__': []}
        index = self.context
        for word in self.words[index:]:
            key = ' '.join(self.words[index-self.context:index])
            if key in rule:
                rule[key].append(word)
            else:
                rule[key] = [word]
            if any(i in self.suffixes for i in word):
                rule['__endings__'].append(word)
            index += 1
        rule['__endings__'] = list(set(rule['__endings__']))
        self.rule = rule

    def load_rule(self, file_name):
        import pickle
        with open(file_name, 'rb') as f:
            self.rule = pickle.load(f)
        self.context = len(list(self.rule.keys())[1].split())

    def save_rule(self, file_name):
        import pickle
        with open(file_name, 'wb') as f:
            pickle.dump(self.rule, f)

    def print_rule(self):
        for k, v in self.rule.items():
            print(f"{k} -> {' '.join(v)}")

    def read_rule(self, file_name):
        rule = {}
        with open(file_name) as f:
            for line in f.readlines():
                raw_line = line.split('->')
                key = raw_line[0].strip()
                value = raw_line[1].strip()
                rule[key] = value
        self.rule = rule
        self.context = len(list(self.rule.keys())[1].split())

    def first_word(self, rule):
        def is_word_start(k):
            return 1 < len(k) and k[0].isupper() and k[1].islower()
        # return random.choice(list(filter(is_word_start, rule.keys()))).split()
        return random.choice(list(rule.keys())).split()

    def next_word(self):
        new_word = self.get_word(self.rule[self.key])
        self.string += new_word + ' '
        for word in range(len(self.old_words)):
            self.old_words[word] = self.old_words[
                (word + 1) % len(self.old_words)
            ]
        self.old_words[-1] = new_word
        self.key = ' '.join(self.old_words)

    def get_word(self, word_choices):
        return random.choice(word_choices)

    def last_word(self):
        def end_with_suffix(k):
            suffixes = ['.', '!', '?']
            return any(i in suffixes for i in k)

        try:
            self.string += random.choice(
                list(filter(end_with_suffix, self.rule[self.key]))
            )
        except IndexError:
            self.next_word()
            self.last_word()

    def make_string(self):
        self.old_words = self.first_word(self.rule)
        self.string = ' '.join(self.old_words) + ' '
        self.key = ' '.join(self.old_words)

        for i in range(self.length):
            self.next_word()
        self.last_word()


def read_data(file):
    with open(file) as f:
        contents = f.read()
    return contents


def parse_input():
    if len(sys.argv) > 1:
        data = read_data(sys.argv[1])
    else:
        print("Please provide a data file")
    if len(sys.argv) > 2:
        context = int(sys.argv[2])
    else:
        context = 2
    if len(sys.argv) > 3:
        length = int(sys.argv[3])
    else:
        length = 10
    if len(sys.argv) > 4:
        loop = int(sys.argv[4])
    else:
        loop = 1
    return (data, context, length, loop)


def main():
    data, context, length, loop = parse_input()
    chain = Markov(data)
    chain.with_context(context)
    chain.with_length(length)
    chain.make_rule()
    for _ in range(loop):
        chain.make_string()
        print(chain.string)

def test_print():
    data, context, length, loop = parse_input()
    chain = Markov(data)
    chain.with_context(context)
    chain.with_length(length)
    chain.make_rule()
    chain.print_rule()

def test_read():
    chain = Markov()
    chain.read_rule("./test_rule")
    chain.make_string()
    print(chain.string)

if __name__ == '__main__':
    main()
    # test_print()
    # test_read()
